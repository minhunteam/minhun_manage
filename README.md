# 테스트 관리자 프로젝트

### **DB 관련 프로퍼티 설정**
/src/db.properties
##### **별도 작성**
db_type=mysql  
driver=com.mysql.jdbc.Driver  
url=jdbc\:mysql\://{서버주소}\:3306/{DB명}?autoReconnect=false&autoReconnectForPools=true&useSSL=false&verifyServerCertificate=false  
username={계정}  
password={패스워드}  
### **PUSH 관련 프로퍼티 설정**
/src/fcm.properties
##### **별도 작성**
fcm_server_key={FCM 서버 키}